var LaysPhoto = {};
LaysPhoto.DESKTOP = 'desktop';
LaysPhoto.TABLET = 'tablet';
LaysPhoto.PHONE = 'phone';
LaysPhoto.size = {
	desktop: {
		width: 1920,
		height: 1080
	},
	tablet: {
		width:768,
		height:1024
	},
	phone: {
		width: 640,
		height: 960
	}
};
LaysPhoto.debug = false;

LaysPhoto.init = function(url) {

	this.saveUrl = url;

	$('.typeBtn').click(this.onClickType.bind(this));
	$('#textInput').change(this.onChangeTextInput.bind(this));

	$('#textInput').focus(function(e){
		if(e.target.value == "ВВЕДИТЕ ТЕКСТ..."){
			e.target.value = '';
		}
	});

	$('#textInput').focusout(function(e){
		if(e.target.value == ""){
			e.target.value = 'ВВЕДИТЕ ТЕКСТ...';
		}
	});

	document.getElementById('textInput').defaultValue = "ВВЕДИТЕ ТЕКСТ...";

	$('#upBtn').click(this.moveUpSlider.bind(this));
	$('#downBtn').click(this.moveDownSlider.bind(this));
	$('#okBtn').click(this.sendResuldImage.bind(this));

	if(window.innerWidth <= 1024){
		$('#desktop').hide();
	} else if(window.innerWidth <= 768){
		this.initRenderer('phone');
	} else {

	}

	$('.footballer').draggable({
		snap: 'canvas',
		helper: "clone",
		scroll: true,
		appendTo: "body",
		zIndex: 100/*,
		start: function(e){
			$(e.originalEvent.target).data("startingScrollTop",$(e.originalEvent.target).parent().scrollTop());
		},
		drag: function(e,ui){
			var st = parseInt($(e.originalEvent.target).data("startingScrollTop"));
			ui.position.top -= $(e.originalEvent.target).parent().scrollTop() - st;
			LaysPhoto.unselectObject();
		}*/
	});

	$('#canvasContainer').droppable({
		//accept: "#canvasContainer",
		drop: this.onDropDecal.bind(this)
	});

	$('#canvasContainer').mouseout(this.unselectObject.bind(this));

	this.slider = $('#decals');
	this.slider.currentSlide = 0;

};

LaysPhoto.onClickType = function(e){

	if(this.renderer){
		return;
	}

	this.initRenderer(e.target.parentNode.id);
};

LaysPhoto.initRenderer = function(type){

	var size = this.size[type]

	this.container = document.getElementById('canvasContainer');
	this.renderer = PIXI.autoDetectRenderer(size.width, size.height, {backgroundColor : 0x000000, alpha: 0, preserveDrawingBuffer: true});
	this.container.appendChild(this.renderer.view);
	this.stage = new PIXI.Container();
	this.stage.width = size.width;
	this.stage.height = size.height;
	//this.container.style.width = size.width + '.px';
	//this.container.style.height = size.height + '.px';

	var texture = PIXI.Texture.fromImage('assets/img/background_' + type + '.png');
	var background = new PIXI.Sprite(texture);

	background.anchor.x = 0.5;
	background.anchor.y = 0.5;
	background.position.x = size.width / 2;
	background.position.y = size.height / 2;
	this.stage.addChild(background);

	this.elementsContainer = new PIXI.Container();
	this.stage.addChild(this.elementsContainer);

	this.transformController = new LaysPhoto.TransformController();
	this.stage.addChild(this.transformController);

	this.transformController.visible = false;
	this.transformController.on('onRemove', this.onRemoveDecal, this);

	this.render();

	$('#typeConatiner').hide();
	$('#editor').show();

};

LaysPhoto.onChangeTextInput = function(e){
	this.addText(e.target.value);
};


LaysPhoto.addText = function(text){

	if(text.length > 0){
		if(!this.text){
			this.text = new PIXI.Text('aa', { font: '41px Circe', fill: 'white', align: 'center', wordWrap: true, wordWrapWidth: 15 });
			this.text.position.set(this.renderer.width / 2, this.renderer.height / 2);
			this.text.interactive = true;
			this.text.tap = this.onSelectDecal.bind(this);
			this.text.click = this.onSelectDecal.bind(this);
			this.elementsContainer.addChild(this.text);
		}

		this.text.text = text.toUpperCase();

		this.selectObject(this.text);
	} else {
		if(this.text){
			this.elementsContainer.removeChild(this.text);
			this.text = null;
		}
	}

};

LaysPhoto.onDropDecal = function(e, ui){

	var xPos = device.desktop() ? e.pageX - e.target.offsetLeft - e.offsetX : e.pageX - e.target.offsetLeft ,
		yPos = device.desktop() ? e.pageY - e.target.offsetTop - e.offsetY : e.pageY - e.target.offsetTop,
		x = this.renderer.width / 100 * (100 * xPos / this.renderer.view.clientWidth),
		y = this.renderer.height / 100 * (100 * yPos / this.renderer.view.clientHeight),
		id = e.originalEvent.target.id,
		_this = this;

	if(!PIXI.loader.resources[id]){
		LaysPhoto.loadDecal(id, function(){
			_this.addDecal(id, x, y);
		});
	} else {
		this.addDecal(id, x, y);
	}

};

LaysPhoto.addDecal = function(id, x, y){

	var imgId = 'assets/img/boys/' + id + '.png';

	var texture = PIXI.Texture.fromImage(imgId),
		decal = new PIXI.Sprite(texture);

	decal.position.set(x, y);
	decal.interactive = true;
	decal.tap = this.onSelectDecal.bind(this);
	decal.click = this.onSelectDecal.bind(this);

	this.elementsContainer.addChild(decal);

	this.selectObject(decal);

};

LaysPhoto.loadDecal = function(id, callback){
	PIXI.loader
			.add(id, 'assets/img/boys/' + id + '.png')
			.load(callback)
};

LaysPhoto.onSelectDecal = function(e){
	this.selectObject(e.target);
};

LaysPhoto.onRemoveDecal = function(obj){

	if(obj.text){
		this.text = null;
	}

	this.elementsContainer.removeChild(obj);
	this.transformController.visible = false;

};

LaysPhoto.isIntersecting = function(x, y, obj) {
	return !(obj.x > x || (obj.x + obj.width) < x || obj.y > y || (obj.y + obj.height) < y);
};

LaysPhoto.selectObject = function(object){
	this.transformController.setObject(object);
	this.transformController.visible = true;
};

LaysPhoto.unselectObject = function(){
	this.transformController.visible = false;
};

LaysPhoto.render = function() {
	requestAnimationFrame(LaysPhoto.render.bind(this));
	this.renderer.render(this.stage);
};

LaysPhoto.moveUpSlider = function() {

	if(!this.slider.isAnim && this.slider.currentSlide < 2){

		this.slider.isAnim = true;

		this.slider.animate({top: '-=287'}, 250, function(){
				this.slider.isAnim = false;
				this.slider.currentSlide += 1;
			}.bind(this));
	}
};

LaysPhoto.moveDownSlider = function() {
	if(!this.slider.isAnim && this.slider.currentSlide > 0){

		this.slider.isAnim = true;

		this.slider.animate({top: '+=287'}, 250, function(){
				this.slider.isAnim = false;
				this.slider.currentSlide -= 1;
			}.bind(this));
	}
};

LaysPhoto.sendResuldImage = function() {

	this.unselectObject();
	this.renderer.render(this.stage);

	if (!confirm("Вы уверены, что хотите сгенерировтаь обои?")) {
		return;
	}

	var ajaxProp = {
			url: window.location.origin + this.saveUrl,
			type: "POST",
			dataType: 'json',
			data: {
				imgBase64: this.getImageBase64()
			},
			context: this,
			statusCode : {
				200: function(){
					this.saveResuldImage();
				}
			}
		};

	if(this.debug){
		this.saveResuldImage();
	} else {
		$.ajax(ajaxProp);
	}

};

LaysPhoto.saveResuldImage = function() {

	var asArray = [],
		i = 0,
		dataURL = this.getImageBase64(),
		data, blob;


	data = atob(dataURL.substring( "data:image/png;base64,".length )),
	asArray = new Uint8Array(data.length);

	for( i = 0, len = data.length; i < len; ++i ) {
		asArray[i] = data.charCodeAt(i);
	}

	blob = new Blob([ asArray.buffer ], {type: "image/png"});

	saveAs(blob, "photo.png");

	location.href = window.location.origin + '/cabinet';

};

LaysPhoto.getImageBase64 = function() {
	return this.renderer.view.toDataURL("image/png");
};