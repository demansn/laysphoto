
var gulp = require('gulp'),
	gls = require('gulp-live-server'),
	uglify = require('gulp-uglify'),
	minify = require('gulp-minify'),
	concat = require('gulp-concat');  

gulp.task('dev', function() {

	var server = gls.static('dev', 8087);
	server.start();

	gulp.watch(['dev/assets/js/**/*.js', 'dev/index.html'], function (file) {
		server.notify.apply(server, [file]);
	});
});

gulp.task('build', function() {  
    gulp.src([
        "dev/assets/js/lib/jquery-1.12.0.min.js",
        "dev/assets/js/lib/jquery-ui.min.js",
        "dev/assets/js/lib/jquery.ui.touch-punch.min.js",
        "dev/assets/js/lib/device.min.js",
        "dev/assets/js/lib/FileSaver.min.js",
        "dev/assets/js/lib/pixi.min.js",
        "dev/assets/js/LaysPhoto.js",
        "dev/assets/js/TransformController.js"
    	])
        .pipe(uglify())
        .pipe(concat('LaysPhoto.js'))      //         
        .pipe(gulp.dest('build/assets/js/'));

});

gulp.task('test', function() {
	var server = gls.static('build', 8087);
	server.start();
});